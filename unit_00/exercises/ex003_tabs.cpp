/**
 * 0-3. The string literal "\t" represents a tab character; different C++ 
 * implementations display tabs in different ways. Experiment with your 
 * implementation to learn how it treats tabs.
*/


#include <iostream>

int main()
{
  std::cout << "This (\") is a quote,\t\t and this(\\) is a backslash.\n";

  return 0;
}