/**
 * 0-5. Is this a valid program? Why or why not?
*/

#include <iostream>
int main()  std::cout << "Hello, world!" << std::endl;

/**
 * answer:
 * No, because the program is not inside a main scope
*/