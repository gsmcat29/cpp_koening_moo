/**
 * 0-6. Is this a valid program? Why or why not?
*/

#include <iostream>
int main() {{{{{{std::cout << "Hello, world!" << std::endl;}}}}}}

/**
 * Answer:
 * Yes, the program compiles, and because we have a scope
*/