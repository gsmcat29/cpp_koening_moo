/**
 * 0-8. ...and this one?
*/

#include <iostream>

int main()
{
  // This is a comment that extends over several lines
  // by using // at the beginning of each line linstead of using /*
  // or */ to delimit comments
  std::cout << "Does this work?" << std::endl;

  return 0;
}

/**
 * Answer:
 * This works, as explained in the comment section
*/