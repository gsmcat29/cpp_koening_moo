/**
 * 2-2 . Change the framing program so that it uses a different amount of space 
 * to separate the sides from the greeting than it uses to separate the top and 
 * bottom borders from the greeting.
*/


#include <iostream>
#include <string>

// say what standard-library names we use
using std::cout;
using std::cin;
using std::endl;
using std::string;

int main()
{
  // ask for the person's name
  cout << "Please enter your first name: ";

  // read the name
  string name;
  cin >> name;

  // build the message that we intend to write
  const string greeting = "Hello, " + name + "!";

  // the number of blanks surrounding the greeting
  const int pad = 1;
  const int lateral = 2;

  // total number of rows to write
  const int rows = pad * 2 + 3;
  const string::size_type cols = greeting.size() + pad * 2 + lateral*2 + 1;

  // write a blank line to separate the output from the input
  cout << endl;

  // write number of rows of ouput
  // invariant we have written r rows so far
  for (int r = 0; r != rows; ++r)
  {
    string::size_type c = 0;
    // invariant: we have written c characters so far in the current row
    while (c != cols)
    {
      //  is it time wo write the greeting?
      if (r == pad + 1 && c == pad + 2)
      {
        cout << greeting;
        c += greeting.size();
      }
      else
      {
        // are we on the border?
        if (r == 0 || r == rows-1 || c == 0 || c == cols-1)
          cout << "*";
        else
          cout << " ";
        
        ++c;
      }
    }

    cout << endl;
  }

  return 0;
}