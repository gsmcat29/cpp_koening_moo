// 2-6. What does the following code do?

/**
 * Answer before compiling:
 * print the values of i
*/

#include <iostream>

int main()
{
  int i = 0;

  while (i < 10)
  {
    i += 1;
    std::cout << i << std::endl;
  }

  return 0;
}

/**
 * Answer after compiling:
 * print the values of i
*/