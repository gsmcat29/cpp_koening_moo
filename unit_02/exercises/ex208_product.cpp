// 2-8. Write a program to generate the product of the numbers in the range 
// [1, 10) .

#include <iostream>

int main()
{
  int product = 1;

  for (int i = 2; i <= 10; i++)
  {
    product = product * i;
  }

  std::cout << "cumulative product of digits: " << product << std::endl;

  return 0;
}