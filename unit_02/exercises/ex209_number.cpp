/*
2-9. Write a program that asks the user to enter two numbers and tells the user 
which number is larger than the other.
*/

#include <iostream>

int main()
{
  int number_0 = 0;
  int number_1 = 0;

  std::cout << "Enter two numbers: ";
  std::cin >> number_0 >> number_1;

  if (number_0 > number_1)
    std::cout << number_0 << " is larger than " << number_1 << std::endl;
  else
    std::cout << number_1 << " is larger than " << number_0 << std::endl;


  return 0;
}