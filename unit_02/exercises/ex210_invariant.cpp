// 2-10. Explain each of the uses of std:: in the following program:

#include <iostream>

int main()
{
  int k = 0;
  int n = 5;
  while (k != n)      // invariant: we have written k asterisk so far
  {
    using std::cout;
    cout << "*";
    ++k;
  }

  std::cout << std::endl; // std: is require here

  return 0;
}

/**
 * The first std::cout is to use cout without the scope operator inside the while
 * The second and third std:: is to call a end of line, due the fact the using
 * is out of scope for those ones. 
 * */